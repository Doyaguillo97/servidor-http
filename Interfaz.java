package servidor;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JTextArea;
import javax.swing.JToggleButton;
import static servidor.Cliente.*;

public class Interfaz extends JFrame {

    JButton mostrar_usuarios, mostrar_ban, aceptar, directorio, mostrar_logs;
    JToggleButton onoff;
    JTextArea AreaMostrarUsu, AreaClientes, AreaMostrarBan, AreaInput, AreaDirectorio;

    private Component ventana;
    public String ban, direccion;

    public Interfaz() {

        setSize(900, 550);
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        setLayout(null);

        onoff = new JToggleButton("Encender\nServidor");
        add(onoff);
        onoff.setBounds(30, 30, 150, 415);
        onoff.setBackground(Color.GREEN);

        mostrar_usuarios = new JButton("Mostrar Usuarios");
        add(mostrar_usuarios);
        mostrar_usuarios.setBounds(440, 290, 170, 40);

        mostrar_logs = new JButton("Mostrar logs");
        add(mostrar_logs);
        mostrar_logs.setBounds(660, 290, 200, 40);

        mostrar_ban = new JButton("Mostrar IP banneada");
        add(mostrar_ban);
        mostrar_ban.setBounds(225, 290, 170, 40);

        aceptar = new JButton("Banear ip");
        add(aceptar);
        aceptar.setBounds(225, 415, 90, 30);

        directorio = new JButton("Seleccionar Directorio");
        add(directorio);
        directorio.setBounds(440, 415, 160, 30);

        AreaMostrarUsu = new JTextArea();
        add(AreaMostrarUsu);
        AreaMostrarUsu.setBounds(440, 30, 200, 250);
        AreaMostrarUsu.setEditable(false);
        AreaMostrarUsu.setText("Mostrar Usuarios");

        AreaClientes = new JTextArea();
        add(AreaClientes);
        AreaClientes.setBounds(660, 30, 200, 250);
        AreaClientes.setEditable(false);
        AreaClientes.setText("Clientes conectados...");

        AreaMostrarBan = new JTextArea();
        add(AreaMostrarBan);
        AreaMostrarBan.setBounds(225, 30, 200, 250);
        AreaMostrarBan.setEditable(false);
        AreaMostrarBan.setText("Mostrar IP banneada");

        AreaInput = new JTextArea();
        add(AreaInput);
        AreaInput.setBounds(225, 360, 200, 40);
        AreaInput.setEditable(true);

        AreaDirectorio = new JTextArea();
        add(AreaDirectorio);
        AreaDirectorio.setBounds(440, 360, 420, 40);
        AreaDirectorio.setEditable(false);
        AreaDirectorio.setText("Direccion...");

        //ACTION LISTENER
        mostrar_usuarios.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                FileReader lector = null;
                try {
                    lector = new FileReader(path + "\\data\\dataServer.txt");
                    BufferedReader bfReader = new BufferedReader(lector);

                    String lineaFichero;
                    StringBuilder contenidoFichero = new StringBuilder();

                    try {
                        // Recupera el contenido del fichero
                        while ((lineaFichero = bfReader.readLine()) != null) {
                            contenidoFichero.append(lineaFichero);
                            contenidoFichero.append("\n");
                        }
                        
                        AreaMostrarUsu.setText(contenidoFichero.toString());
                        //AbrirFichero(AreaMostrarBan);
                    } catch (IOException ex) {
                        Logger.getLogger(Interfaz.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } catch (FileNotFoundException ex) {
                    Logger.getLogger(Interfaz.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        mostrar_logs.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                FileReader lector = null;
                try {
                    lector = new FileReader(path + "\\logs\\logs.txt");
                    BufferedReader bfReader = new BufferedReader(lector);

                    String lineaFichero;
                    StringBuilder contenidoFichero = new StringBuilder();

                    try {
                        // Recupera el contenido del fichero
                        while ((lineaFichero = bfReader.readLine()) != null) {
                            contenidoFichero.append(lineaFichero);
                            contenidoFichero.append("\n");
                        }
                        AreaClientes.setText(contenidoFichero.toString());

                        //AbrirFichero(AreaMostrarBan);
                    } catch (IOException ex) {
                        Logger.getLogger(Interfaz.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } catch (FileNotFoundException ex) {
                    Logger.getLogger(Interfaz.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        onoff.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent evt) {
                if (onoff.isSelected()) {
                    onoff.setBackground(Color.BLACK);
                    onoff.setText("Parar\nServidor");

                } else {
                    onoff.setBackground(Color.GREEN);
                    onoff.setText("Encender\nServidor");
                }
            }
        });

        aceptar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ban = AreaInput.getText();  //CUANDO LE DAS A ACEPTAR GUARDA EN UN STRING

                if (checkFile(ban, path + "\\bans\\bans.txt")) {
                    añadirData(path + "\\bans\\bans.txt", ban);
                }
            }
        });

        mostrar_ban.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                FileReader lector = null;
                try {
                    lector = new FileReader(path + "\\bans\\bans.txt");
                    BufferedReader bfReader = new BufferedReader(lector);

                    String lineaFichero;
                    StringBuilder contenidoFichero = new StringBuilder();

                    try {
                        // Recupera el contenido del fichero
                        while ((lineaFichero = bfReader.readLine()) != null) {
                            contenidoFichero.append(lineaFichero);
                            contenidoFichero.append("\n");
                        }
                        AreaMostrarBan.setText(contenidoFichero.toString());

                        //AbrirFichero(AreaMostrarBan);
                    } catch (IOException ex) {
                        Logger.getLogger(Interfaz.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } catch (FileNotFoundException ex) {
                    Logger.getLogger(Interfaz.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        directorio.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser fileChooser = new JFileChooser(path);
                fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
                if (JFileChooser.APPROVE_OPTION == fileChooser.showOpenDialog(ventana)) {
                    File archivo = fileChooser.getSelectedFile();
                    direccion = archivo.getPath();
                    AreaDirectorio.setText(direccion);

                }
            }
        });
    }

    public void AbrirFichero(JTextArea TextArea) {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        if (JFileChooser.APPROVE_OPTION == fileChooser.showOpenDialog(ventana)) {
            File archivo = fileChooser.getSelectedFile();
            FileReader lector = null;
            try {
                lector = new FileReader(archivo);
                BufferedReader bfReader = new BufferedReader(lector);

                String lineaFichero;
                StringBuilder contenidoFichero = new StringBuilder();

                // Recupera el contenido del fichero
                while ((lineaFichero = bfReader.readLine()) != null) {
                    contenidoFichero.append(lineaFichero);
                    contenidoFichero.append("\n");
                }

                // Pone el contenido del fichero en el area de texto
                TextArea.setText(contenidoFichero.toString());

            } catch (FileNotFoundException ex) {
                System.out.println("Error al abrir el archivo...");
            } catch (IOException ex) {
                System.out.println("Error al abrir el archivo...");
            } finally {
                try {
                    lector.close();
                } catch (IOException ex) {
                    System.out.println("Error al abrir el archivo...");
                }
            }
        }
    }

}
