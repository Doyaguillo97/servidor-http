/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servidor;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.charset.Charset;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 *
 * @author josed
 */
public class Cabecera {
    
    public StringBuffer cabecera;
    public String cod_Error;
    public String date;
    public String content_type;
    public String content_lenght;
    
    public Cabecera(Peticion p, File archivo, boolean error) {
        cabecera = new StringBuffer();
        cod_Error = p.getVersion_http() + " " + error(error);
        date = "Date: " + fecha() + "\r\n";
        content_type = "Content-Type: " + contentType(p.getRecurso(),error) + "\r\n";
        content_lenght = "Content-Lenght: " + archivo.length();
        cabecera.append(cod_Error).append(date).append(content_type).append(content_lenght).append("\r\n\r\n");
        
    }//end constructor

    /*     GETTERS      */
    public StringBuffer getCabecera() {
        return cabecera;
    }
    
    public String getCod_Error() {
        return cod_Error;
    }
    
    public String getDate() {
        return date;
    }
    
    public String getContent_type() {
        return content_type;
    }
    
    public String getContent_lenght() {
        return content_lenght;
    }

    /*      METODOS DE LA CLASE     */
    String error(boolean error) {
        String codigo = "200 OK \r\n";
        if (!error) {
            codigo = "404 Not Found \r\n";
        }
        return codigo;
    }//end method

    public StringBuffer fecha() {
        StringBuffer fecha = new StringBuffer();
        
        Calendar calendar = new GregorianCalendar();
        fecha.append(Integer.toString(calendar.get(calendar.DATE))).append(" ").
                append(Integer.toString(calendar.get(calendar.MONTH))).append(" ").
                append(Integer.toString(calendar.get(calendar.YEAR)));
        return fecha;
    }//end method

    public String contentType(String aux,boolean error) { //MIMEs admitidos: jpg, jpeg, html, htm, gif, ico
        String contentType = null;
        String[] parts = aux.split("\\.");
        if (!error) {
            contentType = "text/html";//ñapita
        } else {
            
            switch (parts[1]) {
                case "html":
                    contentType = "text/html";
                    break;
                
                case "htm":
                    contentType = "text/html";
                    break;
                
                case "jpg":
                    contentType = "image/jpeg";
                    break;
                
                case "jpeg":
                    contentType = "image/jpeg";
                    break;
                
                case "gif":
                    contentType = "image/gif";
                    break;
                
                case "ico":
                    contentType = "image/x-icon";
                    break;
                case "js":
                    contentType = "application/javascript";
                    break;
                case "json":
                    contentType = "application/json";
                    break;
                case "css":
                    contentType = "text/css";
                    break;
                case "txt":
                    contentType = "text/plain";
                    break;
                    
                default:
                    contentType = "text/html";
                    break;
            
            
            }
        }
        return contentType;
    }//end method
}//end class
