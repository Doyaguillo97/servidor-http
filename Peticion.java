package servidor;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.charset.Charset;
import java.util.Calendar;
import java.util.GregorianCalendar;

/* Clase con los diferentes campos de una peticion http*/
public class Peticion {

    public String[] lineas;
    public String[] toks;
    public String[] direccion_recurso;
    public String[] data;
    public String pagina;
    public String verbo;
    public String mensaje;
    public String version_http;
    public String recurso;
    

    public Peticion(String http) {
        mensaje = http;
        lineas = mensaje.split("\n");
        toks = lineas[0].split("\\s+");
        verbo = toks[0];
        pagina = toks[1].substring(1);
        version_http = toks[2];
        direccion_recurso = pagina.split("/"); //separa simplemente en el recurso que se va a utilizar por si esta dentro de diferentes carpetas
        recurso = direccion_recurso[direccion_recurso.length - 1];
        if (verbo.equals("POST")) 
            data = lineas [lineas.length - 1].split("&");// Si el verbo es post almacena los
                                                        // datos que comunique el cliente.  
    }

    public String[] getLineas() {
        return lineas;
    }

    public String[] getToks() {
        return toks;
    }

    public String[] getDireccion_recurso() {
        return direccion_recurso;
    }

    public String[] getData() {
        return data;
    }

    public String getPagina() {
        return pagina;
    }

    public String getVerbo() {
        return verbo;
    }

    public String getMensaje() {
        return mensaje;
    }

    public String getVersion_http() {
        return version_http;
    }

    public String getRecurso() {
        return recurso;
    }

    

}
