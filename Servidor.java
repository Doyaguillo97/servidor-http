/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servidor;

import java.io.*;
import java.net.*;
import java.awt.*;
import javax.swing.*;

public class Servidor {

    public static void main(String[] args) throws Exception {
        /*
            --> Servidor: ServerSocket
            --> Cliente: Socket
                La clase socket es la que desde el punto de vista del cliente inicia una conexion
                Despde el pto de vista del servidor, representa a un cliente que se acaba de conectar
         */
        ServerSocket server = null;
        server = new ServerSocket(80);
        //introducir dentro de otro bucle while(true) para la interdaz
        Interfaz interfaz = new Interfaz(); // si el boton de encendido no funciona en de forma normal ejecutar en modo debug
        // no entendemos por que en uno funciona la interfaz y en el otro no.
        interfaz.setVisible(true);
        while (true) {
            //System.out.println("Entro aqui");
            if (interfaz.onoff.isSelected()) {
                try {
                    System.out.println("Escuchando...");
                    //accept me devuelve un socket, que es el tio que se acaba de conectar
                    //Socket cliente = server.accept();
                    Cliente unCliente = new Cliente(server.accept());

                    System.out.println("Un cliente conectado. ");

                } catch (IOException ex) {
                    System.out.println("Error: " + ex.toString());
                }

            } else {
                try {
                    ServerOut badRequest = new ServerOut(server.accept());// esta clase siempre va a mostrar un error 500
                    System.out.println("Notificando fallo de conecion...");
                } catch (IOException ex) {
                    System.out.println("Error: " + ex.toString());
                }

            }
        }

    }
}
