/*
Esta clase abre un puerto cuando el servidor esta en stand by y cualquier usuario que se conecte
le muestra que el server esta fuera de servicio
*/
package servidor;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.net.Socket;
import java.nio.charset.Charset;
import static servidor.Cliente.path;

/**
 *
 * @author josed
 */
public class ServerOut extends Cliente {
    
    public ServerOut(Socket elcliente) {
        super(elcliente);
    }
     public void run() {

        try {
            byte[] buffer = new byte[1024];
            int nb = -1;
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ByteArrayInputStream bais = new ByteArrayInputStream(buffer);
            Thread.sleep(150);
            while (is.available() > 0) {
                nb = is.read(buffer);
                if (nb > 0) {
                    //1eer Arg el buffer, el 2 desde donde empiezo a leer, 3 cuantos bytes leo
                    baos.write(buffer, 0, nb);
                }
            }

            Peticion mens = new Peticion(new String(baos.toByteArray(), Charset.forName("UTF-8")));
            String ruta = path + "\\" + "500.html";
            ruta = validacion(ruta, ip, true, mens);

            //ruta path existe ip mens
            File archivo = new File(ruta); // va a mostrar al princio la vista inicial del 

            FileInputStream fr = new FileInputStream(archivo);
            BufferedInputStream br = new BufferedInputStream(fr);

            try {
                /*Lectura del fichero por el flujo de datos*/
                ByteArrayOutputStream baosSalidaFichero = new ByteArrayOutputStream();//body de la peticion
                byte[] bufferFichero = new byte[1024];
                while ((nb = fr.read(bufferFichero)) > 0) {
                    baosSalidaFichero.write(bufferFichero, 0, nb);
                }
                Cabecera cabecera = new Cabecera(mens, archivo, true);
                tipoPeticion(mens, cabecera, baosSalidaFichero);

            } catch (Exception e) {
                e.printStackTrace();
            } finally {

                try {
                    if (null != fr) {
                        fr.close();
                    }
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }

        } catch (Exception ex) {
            System.out.println("Error: " + ex.getMessage());
        } finally {
            try {
                os.close();// cerramos la conexion con el servidor
            } catch (Exception ex) {
                System.out.println("Error: " + ex.getMessage());
            }
        }

    }//end run
  
}
